import java.io.*;
import java.util.*;
import java.util.regex.*;

class vmCheck {


    public static String checkPath = "";



    public static class jackFilter implements FilenameFilter {
        public boolean accept(File dir, String name) {
            Pattern jackVMFile = Pattern.compile("([\\w]+)?(\\.)(vm)");
            Matcher nameTest = jackVMFile.matcher(name);
            boolean matched = nameTest.matches();
            if (matched) {
                File toCheck = new File(vmCheck.checkPath, name);
                File jackOG = new File(vmCheck.checkPath, nameTest.group(1) + ".jack");
                return (jackOG.exists() && toCheck.exists());
            } 
            return false;
        }
    }

    private static Boolean skippable(String str) {
        return (str.equals("neg") || str.startsWith("goto") || str.startsWith("if-goto") || str.startsWith("label") || str.equals("not"));
    }


    private static void checkFile(String checkPath, String sourcePath, String filename) {
        int chkLine = 0;
        int srcLine = 0;
        System.out.println("  ==>> checking " + filename);
        try {
            File chkFile = new File(checkPath, filename);
            File srcFile = new File(sourcePath, filename);
            Scanner checkIn = new Scanner(chkFile);
            Scanner sourceIn = new Scanner(srcFile);
            String mismatch = String.format(" %04d | %30s || %-30s | %04d", srcLine, "source   ", "   generated", chkLine);
            System.out.println(mismatch);
            String dashes = new String(new char[30]).replace("\0", "=");
            String boarder = String.format(" %4s | %30s || %30s | %4s", "====", dashes, dashes, "====");
            System.out.println(boarder);

            while (checkIn.hasNextLine() && sourceIn.hasNextLine()) {
                String chkStr = checkIn.nextLine();
                String srcStr = sourceIn.nextLine();
                chkLine += 1;
                srcLine += 1;
                while (skippable(chkStr) && checkIn.hasNextLine()) {
                    chkStr = checkIn.nextLine();
                    chkLine += 1;
                }
                while (skippable(srcStr) && sourceIn.hasNextLine()) {
                    srcStr = sourceIn.nextLine();
                    srcLine += 1;
                }
                if (!chkStr.equals(srcStr)) {
                    mismatch = String.format(" %-4d | %30s || %-30s | %4d", srcLine, srcStr, chkStr, chkLine);
                    System.out.println(mismatch);
                }

            }
            System.out.println(boarder);
            while (checkIn.hasNextLine()) {
                String chkStr = checkIn.nextLine();
                chkLine += 1;
                while (skippable(chkStr) && checkIn.hasNextLine()) {
                    chkStr = checkIn.nextLine();
                    chkLine += 1;
                }
                mismatch = String.format(" %-4s | %30s || %-30s | %4d", "", "", chkStr, chkLine);
                System.out.println(mismatch);
            }
            while (sourceIn.hasNextLine()) {
                String srcStr = sourceIn.nextLine();
                srcLine += 1;
                while (skippable(srcStr) && sourceIn.hasNextLine()) {
                    srcStr = sourceIn.nextLine();
                    srcLine += 1;
                }
                mismatch = String.format(" %-4d | %30s || %-30s | %4s", srcLine, srcStr, "", "");
                System.out.println(mismatch);
            }
            System.out.println(boarder);
            System.out.println();
            checkIn.close();
            sourceIn.close();
        } catch (Exception e) {
            System.out.println("oops exception e" + e.getMessage());
        }
    }

    public static void main(String[] args) {
        vmCheck.checkPath = args[0];
        String sourcePath = args[1];

        try {
            File sourceDir = new File(sourcePath);
            FilenameFilter jf = new jackFilter();
            for (File f : sourceDir.listFiles(jf)) {
                checkFile(vmCheck.checkPath, sourcePath, f.getName());
            }

        } catch (Exception e) {
            System.err.println("oops there's an error lmao");
            System.err.println(e.getMessage());
        }

        

    }

}