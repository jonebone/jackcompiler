echo 'compiling compiler'
cd final_compiler
javac *.java
echo 'running compiler'
DIR=${1:-"jackPrograms/Set_3/Pong"}

java mycompile $DIR
rm -r output
mkdir output
mv *.class output/

echo 'running complete'

cd ..
DIR2=${1:-"final_compiler/jackPrograms/Set_3/Pong"}
DIR3=${2:-"jackSource/Set_3/Pong"}
javac vmCheck.java
java vmCheck $DIR2 $DIR3
