

public class token {


    private tokenType type;
    private String val;

    public token (tokenType type, String val) {
        this.type = type;
        this.val = val;
    }

    public tokenType getType() {
        return type;
    }

    public String getVal() {
        return val;
    }
    
    public String toString() {
        if (type.equals(tokenType.LINEBREAK)) {
            return "\n=>" + "LINE" + " : " + val + "";
        }
        return "<" + type.name() + ", '" + val + "'>";
    }
}