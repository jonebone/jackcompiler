

import java.util.*;
import java.io.*;

public class parser {

    public class parseException extends Exception {
        public parseException () {
            super("Error found in or near line " + line + " near token " + peekNextToken().getVal());
        }

        public parseException (String message) {
            super("Error found in or near line " + line + " near token " + peekNextToken().getVal() + ": " + message);
        }
    }

    private ArrayList<token> tokens;

    private int line= 0;
    private Stack<String> openSymbolStack = new Stack<String>();
    private LinkedHashMap<String, String> symbolTable = new LinkedHashMap<String, String>();

    private String warningString = " !! - WARNINGS - !! \n";

    private FullScopeTable fst;
    private ClassScopeTable cst = null;
    private FuncSymbol fs = null;
    private Symbol codeContext = null;

    private int labelCount = 0;
    private ArrayList<String> code;

    
    public parser(ArrayList<token> tokens, FullScopeTable fst) {
        this.tokens = tokens;
        this.fst = fst;
        this.code = new ArrayList<String>();
        return;
    }


    private token getNextToken() {
        return tokens.remove(0);
    }

    private token peekNextToken() {
        return tokens.get(0);
    }

    private token tokenLookAhead(int lookUp){
        if (lookUp >= tokens.size()) return new token(tokenType.ENDOFFILE, "");
        return tokens.get(lookUp);
    }

    public ClassScopeTable parse() throws parseException{
        token t = peekNextToken();
        while (!t.getVal().equals("class")) {
            if (t.getType() == tokenType.LINEBREAK) {
                parseLinebreak(getNextToken());
                t = peekNextToken();
            } else throw new parseException();
        }
        parseClass();
        if (!warningString.equals(" !! - WARNINGS - !! \n")) System.out.println(warningString);
        return cst;
    }

    public void printSymbolTable() {
        System.out.println("----------------=--------------------");
        System.out.println("   type        -=-   symbol");
        System.out.println("----------------=--------------------");
        for (String k : symbolTable.keySet()) {
            System.out.println(String.format("  %-12s -=-   %s", symbolTable.get(k), k));
        }
    }

    public void writeVMFile(String path) {
        String fName = path + "/" + cst.name + ".vm";
        String codeStr = "";
        for (String s : code) {
            codeStr = codeStr + s + "\n";
        }
        try {
            File classfile = new File(fName);
            while (!classfile.createNewFile()) {
                classfile.delete();
            }
            FileWriter vmWriter = new FileWriter(fName);
            vmWriter.write(codeStr);
            vmWriter.close();
        } catch (Exception e) {
            System.err.println("Could not make file for " + cst.name);
            System.exit(-1);
        }
    }

    // =============================================================================== //
    // ============================== UTIL =========================================== //
    // =============================================================================== //

    private void parseLinebreak(token t) throws parseException {
        try {
            line = Integer.parseInt(t.getVal());
        } catch (Exception e) {
            throw new parseException("Exception caught: " + e.getMessage());
        }
        
    }

    private String bracketFlip(String b) {
        switch (b) {
            case "{": return "}";
            case "[": return "]";
            case "(": return ")";
            case "<": return ">";
            default: return "";
        }
    }

    private void parseBracket(token t) throws parseException {
        if (t.getType() != tokenType.SYMBOL) throw new parseException();
        String s = t.getVal();
        switch (s) {
            case "{": case "[": case "(": case "<":
                openSymbolStack.push(s);
                break;
            case "}": case "]": case ")": case ">":
                String openerToMatch = bracketFlip(openSymbolStack.peek());
                if (!s.equals(openerToMatch)) throw new parseException("Expected " + s + " to close " + openerToMatch);
                else openSymbolStack.pop();
                break;
            default: throw new parseException();
        }
    }

    

    // =============================================================================== //
    // ============================== CLASS ========================================== //
    // =============================================================================== //


    private void parseClass() throws parseException {
        token t = getNextToken();
        if (!t.getVal().equals("class")) throw new parseException();
        // next value after "class" should be identifier for the class name
        t = getNextToken();               
        if (t.getType() != tokenType.IDENTIFIER) throw new parseException();
        String classname = t.getVal();
        cst = new ClassScopeTable(fst, classname);
        codeContext = cst;
        symbolTable.put(classname, "Class");
        // after class name should be open curly bracket symbol
        t = getNextToken();
        if (!t.getVal().equals("{")) throw new parseException();
        openSymbolStack.add(0, "{"); // using a literal here bc we know what it should be
        // after starting {, parse internals until ending } or throw error if EOF before then
        t = peekNextToken();
        tokenType ttype = t.getType();
        while (ttype != tokenType.SYMBOL) {
            switch (ttype) {
                case ENDOFFILE: throw new parseException("reached end of file sooner than expected");
                case LINEBREAK:
                    parseLinebreak(getNextToken());
                    break;
                case KEYWORD:
                    parseClassDeclaration(classname);
                    break;
                default: throw new parseException();
            }
            t = peekNextToken();
            ttype = t.getType();
        }
        // after internals, we expect a closing bracket
        t = getNextToken();
        if (!t.getVal().equals("}")) throw new parseException();
        parseBracket(t);

    }

    private void parseClassDeclaration(String classname) throws parseException {
        token t = getNextToken();
        String tVal = t.getVal();
        switch (tVal) {
            case "static": case "field":
                // parse in-class var declaration here
                parseVarDec(tVal);
                break;
            case "method": case "function":
                // parse in-class method/func here
                parseFuncDec(classname);
                break;
            case "constructor":
                parseConstructorDec(classname);
                // parse constructor here
                break;
            default: throw new parseException("Did not recognize : " + tVal);
        }
    }

    // =============================================================================== //
    // ============================== VARS =========================================== //
    // =============================================================================== //

    private int parseVarDec(String context) throws parseException {
        // following 'static'/'field', we should have <type> <name>
        token t = getNextToken();
        if (t.getType() != tokenType.IDENTIFIER) throw new parseException();
        String varType = t.getVal();
        // after ID'ing type, register one or more variable names of that type to the symbol table
        return parseDecMultiVar(context, varType, 1);
    }

    private int parseDecMultiVar(String context, String varType, int count) throws parseException {
        // this next token should be the name of the variable
        token t = getNextToken();
        if (t.getType() != tokenType.IDENTIFIER) throw new parseException();
        // this ternary operator (below) allows me to use this even outside of class vars if need be
        //String varSymbol = context.equals("") ? t.getVal() : context + "." + t.getVal();
        if (context.equals("static")) cst.addStaticSymbol(t.getVal(), varType);
        else if (context.equals("field")) cst.addFieldSymbol(t.getVal(), varType);
        else if (context.equals("var")) fs.addVar(t.getVal(), varType);
        // after the name should be a symbol, either ',' or ';'
        t = getNextToken();
        switch (t.getVal()) {
            case ",":
                return parseDecMultiVar(context, varType, count + 1);
            case ";":
                return count;
            default: throw new parseException();
        }
    }

    // =============================================================================== //
    // ============================== FUNC =========================================== //
    // =============================================================================== //

    private void parseFuncDec(String classname) throws parseException {
        // after 'function' or 'method' keyword comes IDENTIFIER token for the return type
        token t = getNextToken();
        if (t.getType() != tokenType.IDENTIFIER) throw new parseException("parseFuncDec type token was " + t);
        String fType = t.getVal();
        // after return type comes func name
        t = getNextToken();
        if (t.getType() != tokenType.IDENTIFIER) throw new parseException("parseFuncDec function name token was " + t);
        String fName = t.getVal();
        fs = new FuncSymbol(fName, fType, cst);
        codeContext = fs;
        // next token should be '(' to start params
        t = getNextToken();
        if (!t.getVal().equals("(")) throw new parseException("parseFuncDec Expected ( token, was " + t);
        parseBracket(t);
        // parse params
        LinkedHashMap<String, String> paramStrings = parseParams(fName, new LinkedHashMap<String, String>());

        int funcIndex = code.size();
        fName = classname + "." + fName;
        code.add("function " + fName + " " + 0);
        if (!fName.endsWith(".main")) {
            code.add("push argument 0");
            code.add("pop pointer 0");
        }
        
        
        
        String context_w_params = fName + "(";
        for (String typeStr: paramStrings.values()) {
            context_w_params = context_w_params + typeStr + ", ";
        }
        if (paramStrings.size() >= 1) context_w_params = context_w_params.substring(0, context_w_params.length() - 2);
        context_w_params = context_w_params + ")";
        symbolTable.put(context_w_params, fType);
        for (String k : paramStrings.keySet()) {
            symbolTable.put(k, paramStrings.get(k));
        }
        // parse body
        parseBody(fName, fType, funcIndex);
        if (fs.getStatic()) {
            code.remove(funcIndex + 1);
            code.remove(funcIndex + 1);
        }
        codeContext = cst;
    }

    private void parseConstructorDec(String classname) throws parseException {
        // constructor keyword will always be followed by classname 'new' (params) {body}
        token t = getNextToken();
        if (!t.getVal().equals(classname)) throw new parseException("constructor does not match class name");
        t = getNextToken();
        if (!t.getVal().equals("new")) throw new parseException("Expected keyword 'new' to follow class name");
        //symbolTable.put(classname + ".new", classname);
        fs = new FuncSymbol(classname, cst);
        codeContext = fs;
        // next token should be '(' to start params
        t = getNextToken();
        if (!t.getVal().equals("(")) throw new parseException();
        parseBracket(t);
        // parse params
        LinkedHashMap<String, String> paramStrings = parseParams(classname + ".new", new LinkedHashMap<String, String>());

        int funcIndex = code.size();
        code.add("function " + classname + ".new " + 0);
        code.add("push constant " + cst.getNumFields());
        code.add("call Memory.alloc 1");
        code.add("pop pointer 0");
        
        String context_w_params = classname + ".new(";
        for (String typeStr: paramStrings.values()) {
            context_w_params = context_w_params + typeStr + ", ";
        }
        if (paramStrings.size() >= 1) context_w_params = context_w_params.substring(0, context_w_params.length() - 2);
        context_w_params = context_w_params + ")";
        symbolTable.put(context_w_params, classname);
        for (String k : paramStrings.keySet()) {
            symbolTable.put(k, paramStrings.get(k));
        }
        // parse body
        parseBody(classname + ".new", "this", funcIndex);
        codeContext = cst;
    }

    private LinkedHashMap<String, String> parseParams(String context, LinkedHashMap<String, String> L) throws parseException {
        token t = getNextToken();
        while (t.getType() == tokenType.LINEBREAK) {
            parseLinebreak(t);
            t = getNextToken();
        }
        if (t.getVal().equals(")")) {
            parseBracket(t);
            return L;
        }
        // two IDENTIFIERs, the type and then the name 
        if (t.getType() != tokenType.IDENTIFIER) throw new parseException();
        String paramType = t.getVal();
        t = getNextToken();
        if (t.getType() != tokenType.IDENTIFIER) throw new parseException();
        L.put(context + "." + t.getVal(), paramType);
        fs.addArg(t.getVal(), paramType);
        // if next token is a comma, call self again.
        t = getNextToken();
        switch (t.getVal()){
            case ",":
                parseParams(context, L);
                break;
            case ")":
                parseBracket(t); 
                return L;
            default: throw new parseException();
        }
        return L;
    }

    // =============================================================================== //
    // =========================== BODY PARTS ======================================== //
    // =============================================================================== //

    private void parseBody(String context, String returnType, int funcIndex) throws parseException {
        token t = getNextToken();
        // deal w any new lines between naming of method and body of method
        while (!t.getVal().equals("{")) {
            if (t.getType() == tokenType.LINEBREAK) {
                parseLinebreak(t);
                t = getNextToken();
            } else throw new parseException();
        }
        parseBracket(t);

        bodyRecursor(context, returnType, funcIndex, 0);
    }

    private void bodyRecursor(String context, String returnType, int funcIndex, int memNum) throws parseException {
        token t = getNextToken();
        while (t.getType() == tokenType.LINEBREAK) {
            parseLinebreak(t);
            t = getNextToken();
        }
        if (t.getVal().equals("}")) {
            parseBracket(t);
            cst.addFuncSymbol(fs);
            return;
        }
        switch(t.getVal()) {
            case "var":
                memNum += parseVarDec("var");
                code.set(funcIndex, "function " + context + " " + memNum);
                break;
            case "let":
                parseLet();
                break;
            case "do":
                parseDo();
                break;
            case "if":
                parseIf(context, returnType, funcIndex, memNum);
                break;
            case "while":
                String loopLabel = "WHILE_START" + labelCount;
                labelCount += 1;
                code.add("label " + loopLabel);
                String condLabel = parseCondition(context, returnType, funcIndex, memNum);
                code.add("goto " + loopLabel);
                code.add("label " + condLabel);
                break;
            case "return":
                parseReturn(context, returnType);
                break;
            default: throw new parseException("unrecognized keyword '" + t.getVal() + "'");
        }
        bodyRecursor(context, returnType, funcIndex, memNum);
        return;
    }

    private void parseDo() throws parseException {
        // should be identifier . identifier (params) ;
        token t = getNextToken();
        if (t.getType() != tokenType.IDENTIFIER) throw new parseException();
        String toCall = t.getVal();
        int expStart = 0;
        t = getNextToken();
        if (t.getVal().equals(".")) {
            t = getNextToken();
            if (t.getType() != tokenType.IDENTIFIER) throw new parseException();
            String idLoc = ((FuncSymbol) codeContext).idLoc(toCall);
            if (!idLoc.equals("")) {
                if (!idLoc.startsWith("static")) ((FuncSymbol) codeContext).nullStatic();
                code.add("push " + idLoc);
                toCall = ((FuncSymbol) codeContext).idType(toCall);
                expStart = 1;
            }
            toCall = toCall + "." + t.getVal();
            t = getNextToken();
        } else {
            toCall = cst.name + "." + toCall;
            code.add("push pointer 0"); // argument of self
            expStart = 1;
            fs.nullStatic();
        }
        if (t.getVal().equals("(")) {
            parseBracket(t);
            t = peekNextToken();
            if (!t.getVal().equals(")")) toCall = toCall + " " + parseExpList(expStart);
            else toCall = toCall + " " + expStart;
            parseBracket(getNextToken());
            t = getNextToken();
            if (!t.getVal().equals(";")) throw new parseException();
            code.add("call " + toCall);
            //if (toCall.startsWith("Output.")) code.add("pop temp 0");
            code.add("pop temp 0");
            return;
        }
        throw new parseException();
    }

    private String parseCondition(String context, String returnType, int funcIndex, int memNum) throws parseException {
        token t = getNextToken();
        if (!t.getVal().equals("(")) throw new parseException();    // (
        parseBracket(t);
        parseExpression();                                          // expression
        t = getNextToken();
        if (!t.getVal().equals(")")) throw new parseException();    // )
        parseBracket(t);

        code.add("not");
        String condLabel = "COND" + labelCount;
        labelCount += 1;
        code.add("if-goto " + condLabel);

        t = getNextToken();
        while (!t.getVal().equals("{")) {
            if (t.getType() == tokenType.LINEBREAK) {
                parseLinebreak(t);                                  // consume any whitespace
                t = getNextToken();
            } else throw new parseException("Expected '{' to begin body, instead found " + t.getVal());
        }
        parseBracket(t);                                            // {
        bodyRecursor(context, returnType, funcIndex, memNum);                          // body, }

        return condLabel;
    }

    private void parseIf(String context, String returnType, int funcIndex, int memNum) throws parseException {
        // gramatically, if is just while with a possibility of else afterwards
        String condLabel = parseCondition(context, returnType, funcIndex, memNum);
        String endLabel = "IF_END" + labelCount;
        labelCount += 1;
        code.add("goto " + endLabel);
        code.add("label " + condLabel);

        token t = peekNextToken();
        while (t.getType() == tokenType.LINEBREAK) {
            parseLinebreak(getNextToken());
            t = peekNextToken();
        }
        if (t.getVal().equals("else")) {
            t = getNextToken();                                         // burn peeked
            t = getNextToken();
            while (!t.getVal().equals("{")) {
                if (t.getType() == tokenType.LINEBREAK) {
                    parseLinebreak(t);                                  // consume any whitespace
                    t = getNextToken();
                } else throw new parseException("Expected '{' to follow 'else', instead found " + t.getVal());
            }
            parseBracket(t);                                            // {
            bodyRecursor(context, returnType, funcIndex, memNum);       // body, }
        }

        code.add("label " + endLabel);
    }

    

    // =============================================================================== //
    // =========================== RETURN TYPE CHECKING ============================== //
    // =============================================================================== //

    private void parseReturn(String context, String returnType) throws parseException {
        token t = peekNextToken();
        if (returnType.equals("this")) {
            if (!t.getVal().equals("this")) throw new parseException("Constructor should return 'this'");
            code.add("push pointer 0");
            code.add("return");
            t = getNextToken(); // this
            t = getNextToken();
            if (!t.getVal().equals(";")) throw new parseException("Expected ; to follow return statement");
            return;
        }
        if (returnType.equals("void") && t.getVal().equals(";")) {
            code.add("push constant 0");
            code.add("return");
            t = getNextToken();
            return;
        }
        if (returnType.equals("void") && !t.getVal().equals(";"))  throw new parseException("does not return void when void expected");
        if (returnType.equals("void") && !t.getVal().equals(";")) {
            warningString += "-> Warning at or near line " + line + ": does not return void when expected \n";
            return;
        }
        tokenType ttype = t.getType();

        if (ttype == tokenType.IDENTIFIER && tokenLookAhead(1).getVal().equals(";")) {
            String idVal = t.getVal();
            if (symbolTable.containsKey(context + "." + idVal)) {
                if (symbolTable.get(context + "." + idVal).equals(returnType)) {
                    warningString += "-> Warning at or near line " + line + ": "+ idVal +" not initialized as return type "+ returnType +" \n";
                }
                t = getNextToken();     // burn peeked
                String retLoc = ((FuncSymbol) codeContext).idLoc(t.getVal());
                if (retLoc.equals("")) throw new parseException("unknown symbol");
                code.add("push " + retLoc);
                code.add("return");
                t = getNextToken();
                if (!t.getVal().equals(";")) throw new parseException("Expected ';' after returned value, found " + t.getVal());
                return;
            }
            if (!typeMatches(idVal, context, returnType)) {
                warningString += "-> Warning at or near line " + line + ": "+ idVal +" not initialized as return type "+ returnType +" \n";
            }
            t = getNextToken();     // burn peeked
            String retLoc = ((FuncSymbol) codeContext).idLoc(t.getVal());
            if (retLoc.equals("")) throw new parseException("unknown symbol");
            if (!retLoc.startsWith("static")) fs.nullStatic();
            code.add("push " + retLoc);
            code.add("return");
            t = getNextToken();
            if (!t.getVal().equals(";")) throw new parseException("Expected ';' after returned value, found " + t.getVal());
            return;
        }

        parseExpression();
        code.add("return");
        t = getNextToken();
        if (!t.getVal().equals(";")) throw new parseException("Expected ';' after returned value, found " + t.getVal());
    }
    

    private boolean contextMatches(String context, String test) throws parseException {
        String[] testElements = test.split("\\.");
        String[] contextElements = context.split("\\.");
        if (testElements.length == 0 || contextElements.length == 0) throw new parseException("testElements length " + testElements.length + ", contextElements length " + contextElements.length);
        String lastInTest = testElements[testElements.length - 1];
        testElements[testElements.length - 1] = lastInTest.split("\\(")[0];
        String testEnd = testElements[testElements.length - 1];
        String contextEnd = contextElements[contextElements.length - 1];
        if (!testEnd.equals(contextEnd)) {
            return false;
        }
        int iterationLimit = testElements.length < contextElements.length ? testElements.length - 1 : contextElements.length - 1;
        String testBuiltString = "";
        String contextBuiltString = "";
        for (int i = 0; i < iterationLimit; i ++) {
            testBuiltString = testBuiltString + testElements[i] + ".";
            contextBuiltString = contextBuiltString + contextElements[i] + ".";
            if (!testBuiltString.equals(contextBuiltString)) return false;
        }
        return true;
    }

    private boolean typeMatches(String idVal, String context, String returnType) throws parseException {
        for (String k : symbolTable.keySet()) {
            if (contextMatches(context + "." + idVal, k)) {
                if (symbolTable.get(k).equals(returnType)) return true;
            }
        }
        return false;
    }
    



    // =============================================================================== //
    // ============================= EXPRESSIONS ===================================== //
    // =============================================================================== //

    private void parseLet() throws parseException {
        token t = getNextToken();
        if (t.getType() != tokenType.IDENTIFIER) throw new parseException();
        String letName = ((FuncSymbol) codeContext).idLoc(t.getVal());
        if (!letName.startsWith("static")) ((FuncSymbol) codeContext).nullStatic();
        t = getNextToken();
        if (t.getVal().equals("[")) {
            parseBracket(t);
            parseExpression();
            t = getNextToken();
            parseBracket(t);
            code.add("push " + letName);
            code.add("add");
            letName = "that 0";
            t = getNextToken();
        }
        if (t.getVal().equals("=")) {
            parseExpression();
            t = getNextToken();
            if (!t.getVal().equals(";")) throw new parseException("expected ; at end of let");
            if (letName.equals("that 0")) {
                code.add("pop temp 0");
                code.add("pop pointer 1");
                code.add("push temp 0");
            }
            code.add("pop " + letName);
            return;
        }
        throw new parseException();
    }

    private int parseExpList(int count) throws parseException {
        parseExpression();
        token t = peekNextToken();
        if (t.getVal().equals(",")) {
            t = getNextToken();
            count = parseExpList(count);
        }
        return count + 1;
    }

    private void parseExpression() throws parseException {
        parseRelational();
        token t = peekNextToken();
        if (t.getVal().equals("&") || t.getVal().equals("|")){
            t = getNextToken();
            parseRelational();
            code.add(t.getVal().equals("&") ? "and" : "or");
        }
    }

    private void parseRelational() throws parseException {
        parseArithmetic();
        token t = peekNextToken();
        if (t.getVal().equals(">") || t.getVal().equals("<") || t.getVal().equals("=")) {
            t = getNextToken();
            parseArithmetic();
            String tVal = t.getVal();
            if (tVal.equals("=")) code.add("eq");
            else code.add(tVal.equals(">") ? "gt" : "lt");
        }
    }

    private void parseArithmetic() throws parseException {
        parseTerm();
        token t = peekNextToken();
        if (t.getVal().equals("+") || t.getVal().equals("-")) {
            t = getNextToken(); // will be either + or - token
            parseTerm();
            code.add((t.getVal().equals("+") ? "add" : "sub"));
        }
    }

    private void parseTerm() throws parseException {
        parseFactor();
        token t = peekNextToken();
        if (t.getVal().equals("*") || t.getVal().equals("/")) {
            t = getNextToken(); // will be either * or / token
            parseFactor();
            String mathStr = "call Math." + (t.getVal().equals("*") ? "multiply" : "divide") + " 2";
            code.add(mathStr);
        }
    }

    private void parseFactor() throws parseException {
        token t = peekNextToken();
        if (t.getVal().equals("~") || t.getVal().equals("-")) {
            t = getNextToken();
            parseOperand();
            code.add(t.getVal().equals("-") ? "neg" : "not");
            return;
        }
        parseOperand();
        
    }

    private void parseOperand() throws parseException {
        token t = getNextToken();
        if (t.getVal().equals("(")) {
            parseBracket(t);
            parseExpression();
            t = getNextToken();
            parseBracket(t);
            return;
        }
        tokenType ttype = t.getType();
        tokenType[] accepted = {tokenType.IDENTIFIER, tokenType.BOOLEANLITERAL, tokenType.INTNUMBER, tokenType.CHARLITERAL, tokenType.STRLITERAL, tokenType.NULLLITERAL, tokenType.THISLITERAL};
        if (!Arrays.asList(accepted).contains(ttype)) throw new parseException();
        if (ttype == tokenType.IDENTIFIER) {
            if (peekNextToken().getType() == tokenType.SYMBOL && (peekNextToken().getVal().equals(".") || peekNextToken().getVal().equals("(") || peekNextToken().getVal().equals("["))) {
                parseOpFollower(peekNextToken(), t.getVal());
            } else {
                String loc = codeContext.isFunc() ? ((FuncSymbol) codeContext).idLoc(t.getVal()) : ((ClassScopeTable) codeContext).idLoc(t.getVal());
                code.add("push " + loc);
                if (!loc.equals("") && !loc.startsWith("static") && codeContext.isFunc()) fs.nullStatic();
            }
        } else if (ttype == tokenType.STRLITERAL) {
            String str = t.getVal();
            char[] chars = str.toCharArray();
            code.add("push constant " + str.length());
            code.add("call String.new 1");
            for (char c : chars) {
                int cint = c;
                code.add("push constant " + cint);
                code.add("call String.appendChar 2");
            }

        } else {
            if (t.getVal().equalsIgnoreCase("TRUE")) code.add("push constant 0\nnot");
            else if (t.getVal().equalsIgnoreCase("FALSE")) code.add("push constant 0");
            else if (t.getVal().equalsIgnoreCase("NULL")) code.add("push constant 0");
            else if (t.getVal().equalsIgnoreCase("this")) {
                code.add("push pointer 0");
                if (codeContext.isFunc()) fs.nullStatic();
            }
            else code.add("push constant " + t.getVal());
        }
        
    }

    private void parseOpFollower(token t, String opStr) throws parseException {
        String tNext = t.getVal();
        switch(tNext) {
            case ".":
                t = getNextToken(); // burn peeked
                t = getNextToken(); // t should be identifier
                opStr = opStr + "." + t.getVal();
                if (t.getType() != tokenType.IDENTIFIER) throw new parseException();
                parseOpFollower(peekNextToken(), opStr);
                break;
            case "(":
                int plusOne = 0;
                if (opStr.contains(".")) {
                    int pi = opStr.indexOf(".");
                    String caller = opStr.substring(0, pi);
                    String callLoc = codeContext.isClass() ? cst.idLoc(caller) : fs.idLoc(caller);
                    if (!callLoc.equals("")) {
                        code.add("push " + callLoc);
                        plusOne = 1;
                        opStr = (codeContext.isClass() ? cst.idType(caller) : fs.idType(caller)) + opStr.substring(pi, opStr.length());
                    }
                }
                t = getNextToken(); // burn peeked
                parseBracket(t);
                t = peekNextToken();
                int numParams = plusOne;
                if (!t.getVal().equals(")")) numParams = parseExpList(plusOne);
                t = getNextToken();
                parseBracket(t);
                if (opStr.equals(".length")) code.add("call String.length 1");
                else code.add("call " + opStr + " " + numParams);
                if (opStr.startsWith("Output.")) code.add("pop temp 0");
                parseOpFollower(peekNextToken(), "");
                break;
            case "[":
                t = getNextToken(); // burn peeked
                parseBracket(t);
                parseExpression();
                t = getNextToken();
                parseBracket(t);
                if (!opStr.contains(".")) code.add("push " + (codeContext.isFunc() ? ((FuncSymbol) codeContext).idLoc(opStr) : ((ClassScopeTable) codeContext).idLoc(opStr)));
                else code.add("push " + opStr);
                code.add("add");
                code.add("pop pointer 1");
                code.add("push that 0");
                parseOpFollower(peekNextToken(), "");
                break;
            default: return;
        }
        return;
    }


    



}