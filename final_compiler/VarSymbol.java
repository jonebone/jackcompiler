

public class VarSymbol extends Symbol {
    private int offset;

    public VarSymbol(String name, String type, Symbol.Kind kind, int offset) {
        super(name, type, kind);
        this.offset = offset;
    }

    public String printString() {
        return String.format("  %-18s || %-10s | %-8s | %-3s |\n", name, type, kind.toString().toLowerCase(), offset);
    }

    public int getOffset() {
        return offset;
    }


}