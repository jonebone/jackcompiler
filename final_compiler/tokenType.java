
import java.util.regex.*;

// https://www.vogella.com/tutorials/JavaRegularExpressions/article.html
// https://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html

public enum tokenType {
    KEYWORD("([a-zA-Z]+)(\\w*)"),
    IDENTIFIER("[a-zA-Z[_]]+(\\w*)"),
    BOOLEANLITERAL("(?i)TRUE|FALSE"),
    NULLLITERAL("(?i)NULL"),
    THISLITERAL("(?i)THIS"),
    CHARLITERAL("(?s)'.'"),
    INTNUMBER("\\d+"),
    SYMBOL("\\p{Punct}"),
    STRLITERAL("\""),        // check after if it contains newline char, if it does do error shit
    OPERATOR("[\\Q-~*/+-><=&|\\E]"),                 // maybe just remove operator and have one symbol token
    ENDOFFILE("(?s)$"),
    LINEBREAK(".");


    public final String patternRegex;

    public Pattern getPattern() {
        return Pattern.compile(this.patternRegex);
    }

    private tokenType(String patternRegex) {
        this.patternRegex = patternRegex;
    }
}