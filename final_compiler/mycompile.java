

import java.io.*;
import java.text.ParseException;
import java.util.*;
import java.util.regex.*;



public class mycompile {
    private static final String keywordString = "class, let, do, if, else, while, return, method, function, constructor, field, static, var";
    public static final Set<String> keywords = new HashSet<String>(Arrays.asList(keywordString.split(", ")));

    private static ArrayList<token> lexer(String filePath) {
        ArrayList<token> tokens = new ArrayList<token>();
        try {
            File inputFile = new File(filePath);
            Scanner sc = new Scanner(inputFile);
            sc.useDelimiter("\\b|(?=\\s)|(?=\\p{Punct})");

            int line = 1;

            tokens.add(new token(tokenType.LINEBREAK, String.valueOf(line)));
            line += 1;

            while (sc.hasNext()) {
                if (sc.hasNext("\\n")) {
                    sc.next();
                    tokens.add(new token(tokenType.LINEBREAK, String.valueOf(line)));
                    line += 1;
                } else if (sc.hasNext("\\s")) {
                    sc.skip("\\s");
                } else if (sc.hasNext("\\/")) {     // Comment Handling
                    String comm = sc.next();
                    if (sc.hasNext("[^\\*/]")) {
                        tokens.add(new token(tokenType.OPERATOR, comm));
                    } else {
                        if (sc.next().equals("/")) {
                            comm = sc.findInLine(".*");
                            //System.out.println("Removed comment: //" + comm);
                        } else {
                            comm = sc.findWithinHorizon("(?s).*?\\*/", 0);
                            //System.out.println("Removed multiline commment: /*" + comm);
                            line += comm.split("\\n").length - 1;       // <<---------- keeps line count appropriately updated if there are multiple lines in comment
                        }
                    }
                }  else if (sc.hasNext(tokenType.STRLITERAL.patternRegex)) {
                    sc.next();
                    String strval = sc.findWithinHorizon("(?s).*?\"", 0);
                    if (strval.contains("\n")) {
                        System.out.println("Multi-Line String found starting on line " + line);
                        line += strval.split("\\n").length - 1;         // <<---------- keeps line count appropriately updated if there are multiple lines in string
                    } else {
                        tokens.add(new token(tokenType.STRLITERAL, strval.substring(0, strval.length() - 1)));
                    }
                } else if (sc.hasNext(tokenType.INTNUMBER.patternRegex)) {
                    tokens.add(new token(tokenType.INTNUMBER, sc.next(tokenType.INTNUMBER.patternRegex)));
                } else if (sc.hasNext(tokenType.BOOLEANLITERAL.patternRegex)) {
                    tokens.add(new token(tokenType.BOOLEANLITERAL, sc.next()));
                } else if (sc.hasNext(tokenType.NULLLITERAL.patternRegex)) {
                    tokens.add(new token(tokenType.NULLLITERAL, sc.next()));
                } else if (sc.hasNext(tokenType.THISLITERAL.patternRegex)) {
                    tokens.add(new token(tokenType.THISLITERAL, sc.next()));
                } else if (sc.hasNext("'")) {
                    String s = sc.next();
                    String chardef = sc.findWithinHorizon("(?s)\\\\?.'", 3);
                    if (chardef != null) {
                        tokens.add(new token(tokenType.CHARLITERAL, s + chardef));
                    }
                } else if (sc.hasNext(tokenType.KEYWORD.patternRegex)) {
                    String wordVal = sc.next(tokenType.KEYWORD.patternRegex);
                    if (!keywords.contains(wordVal)) {           // <<========================================== replace this with some kinda keyword check
                        tokens.add(new token(tokenType.IDENTIFIER, wordVal));
                    } else {
                        tokens.add(new token(tokenType.KEYWORD, wordVal));
                    }
                } else if (sc.hasNext(tokenType.IDENTIFIER.patternRegex)) {
                    tokens.add(new token(tokenType.IDENTIFIER, sc.next(tokenType.IDENTIFIER.patternRegex)));
                } else if (sc.hasNext(tokenType.OPERATOR.patternRegex)) {
                    String opVal = sc.next(tokenType.OPERATOR.patternRegex);
                    tokens.add(new token(tokenType.OPERATOR, opVal));
                } else if (sc.hasNext(tokenType.SYMBOL.patternRegex)) {
                    tokens.add(new token(tokenType.SYMBOL, sc.next()));
                } else {
                    System.out.println("!!! unkown value encountered -" + sc.next() + "- !!!");
                }
            }   // end of while

            tokens.add(new token(tokenType.ENDOFFILE, ""));
            sc.close();
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found exception caught");
            e.printStackTrace();
        }
        
        return tokens;
    }

    public static void compileDirectory(String dirPath) {
        ArrayList<token> t;
        FullScopeTable fst = new FullScopeTable();
        Pattern jackFile = Pattern.compile("([\\w]+)?(\\.)(jack)");
        try {
            File dir = new File(dirPath);
            for (File f : dir.listFiles()) {
                Matcher nameTest = jackFile.matcher(f.getName());
                if (nameTest.matches()) {
                    t = lexer(f.getAbsolutePath());
                    parser p = new parser(t, fst);
                    fst.addClass(p.parse());
                    p.writeVMFile(dirPath);
                }
            }
            fst.outToFiles(dirPath);
        } catch (Exception e) {
            System.out.println("Caught exception: " + e.getMessage());
            e.printStackTrace();
        }
        //fst.printTable();
    }



    public static void main( String[] args )
    {   
        compileDirectory(args[0]);
    }
}
