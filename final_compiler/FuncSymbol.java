

import java.util.*;

public class FuncSymbol extends Symbol {
    private ClassScopeTable parent;
    private Map<String, Symbol> subScope;
    private int argOff;
    private int varOff;
    private Boolean isStatic;
    

    public FuncSymbol(String name, String type, ClassScopeTable parent) {
        super(name, type, Symbol.Kind.METHOD);
        subScope = new HashMap<String, Symbol>();
        argOff = 0;
        varOff = 0;
        this.parent = parent;
        isStatic = true;
        subScope.put("this", new VarSymbol("this", parent.name, Symbol.Kind.ARGUMENT, 0));
        argOff ++;
    }

    // for constructors
    public FuncSymbol(String type, ClassScopeTable parent) {
        super("new", type, Symbol.Kind.METHOD);
        subScope = new HashMap<String, Symbol>();
        argOff = 0;
        varOff = 0;
        this.parent = parent;
        isStatic = true;
    }

    

    public Boolean getStatic() {
        return isStatic;
    }

    public void nullStatic() {
        isStatic = false;
    }

    public Boolean symbolAccessible(String s) {
        return subScope.containsKey(s) ? true : parent.symbolAccessible(s);
    }

    public void addArg(String name, String type) {
        subScope.put(name, new VarSymbol(name, type, Symbol.Kind.ARGUMENT, argOff));
        argOff ++;
    }

    public void addVar(String name, String type) {
        subScope.put(name, new VarSymbol(name, type, Symbol.Kind.VAR, varOff));
        varOff ++;
    }

    public String idLoc(String identifier) {
        if (subScope.keySet().contains(identifier)) {
            Symbol s = subScope.get(identifier);
            if (s.isFuncProperty()) return (s.kind.equals(Kind.ARGUMENT) ? "argument" : "local") + " " + ((VarSymbol) s).getOffset();
        }
        return parent.idLoc(identifier);
    }

    public String idType(String identifier) {
        if (subScope.containsKey(identifier)) {
            Symbol s = subScope.get(identifier);
            if (s.isFuncProperty()) return s.type;
        }
        return parent.idType(identifier);
    }

    protected class SortVars implements Comparator<Symbol> {
        public int compare(Symbol a, Symbol b) {
            if (a.kind == b.kind && a instanceof VarSymbol && b instanceof VarSymbol) {
                return ((VarSymbol) a).getOffset() - ((VarSymbol) b).getOffset();
            }
            return a.kind.compareTo(b.kind);
        }
    }

    public String printString() {
        List<Symbol> vals = new ArrayList<>(subScope.values());
        Collections.sort(vals, new SortVars());
        String str = "\n| " + name + " - method -> "+ type +" - - - - - - - - - - - - - - - - - - - - - -";
        str = str.substring(0, 49) + "\n";
        for (Symbol s : vals) {
            str = str + "| | " + s.printString();
        }
        str = str + "|  - - - - - - - - - - - - - - - - - - - - - - -\n";
        return str;
    }


}