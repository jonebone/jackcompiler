


public abstract class Symbol {
    protected enum Kind {
        CLASS,
        TYPE,
        STATIC,
        FIELD,
        ARGUMENT,
        VAR,
        METHOD
    }

    


    protected String name;
    protected String type;
    protected Kind kind;

    public Symbol(String name, String type, Kind kind) {
        this.name = name;
        this.type = type;
        this.kind = kind;
    }

    public Boolean isType() {
        return kind == Kind.TYPE;
    }

    public Boolean isClassProperty() {
        return kind == Kind.STATIC || kind == Kind.FIELD;
    }

    public Boolean isFuncProperty() {
        return kind ==  Kind.ARGUMENT || kind == Kind.VAR;
    }

    public Boolean isField() {
        return kind == Kind.FIELD;
    }

    public Boolean isFunc() {
        return kind == Kind.METHOD;
    }

    public Boolean isClass() {
        return kind == Kind.CLASS;
    }

    public abstract String printString();
}