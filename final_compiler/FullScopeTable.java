

import java.io.*;
import java.util.*;

class FullScopeTable {
    private Map<String, Symbol> symbols;

    public FullScopeTable() {
        symbols = new HashMap<String, Symbol>();
    }

    public Boolean symbolAccessible(String s) {
        return symbols.containsKey(s);
    }

    public void addClass(String name) {
        symbols.put(name, new ClassScopeTable(this, name));
    }

    public void addClass(ClassScopeTable cst) {
        symbols.put(cst.name, cst);
    }

    private void addBasics() {
        symbols.put("int", new TypeSymbol("int"));
        symbols.put("char", new TypeSymbol("char"));
        symbols.put("String", new TypeSymbol("String"));
        symbols.put("Array", new TypeSymbol("Array"));
    }

    public void printTable() {
        System.out.println();
        for (Symbol s : symbols.values()) {
            System.out.println();
            System.out.println(s.printString());
        }
    }

    public void outToFiles(String path) {
        for (Symbol s : symbols.values()) {
            printClassFile(s, path);
        }
    }

    private void printClassFile(Symbol s, String path) {
        String fName = path + "/" + s.name + "-symbols.txt";
        try {
            File classfile = new File(fName);
            while (!classfile.createNewFile()) {
                classfile.delete();
            }
            System.out.println("Created " + s.name + " symbol file");
            FileWriter symWriter = new FileWriter(fName);
            symWriter.write(s.printString());
            symWriter.close();
        } catch (Exception e) {
            System.err.println("Could not make file for " + s.name);
            System.exit(-1);
        }
    }

}