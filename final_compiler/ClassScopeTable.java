

import java.util.*;
import java.util.stream.Collectors;

public class ClassScopeTable extends Symbol {
    private FullScopeTable parent;
    private Map<String, Symbol> symbols;
    private int staticOffset;
    private int fieldOffset;



    public ClassScopeTable(FullScopeTable parent, String name) {
        super(name, "class", Symbol.Kind.CLASS);
        this.parent = parent;
        symbols = new HashMap<String, Symbol>();
        staticOffset = 0;
        fieldOffset = 0;
    }

    public Boolean containsKey(String k) {
        return symbols.containsKey(k);
    }

    public Boolean containsFunc(String f) {
        return (symbols.containsKey(f) && symbols.get(f).isFunc());
    }

    public String idLoc(String identifier) {
        if (symbols.containsKey(identifier)) {
            Symbol s = symbols.get(identifier);
            if (s.isClassProperty()) return (s.kind.equals(Kind.FIELD) ? "this" : "static") + " " + ((VarSymbol) s).getOffset();
        }
        return "";
    }

    public String idType(String identifier) {
        if (symbols.containsKey(identifier)) {
            Symbol s = symbols.get(identifier);
            if (s.isClassProperty()) return s.type;
        }
        return "";
    }

    public Boolean symbolAccessible(String s) {
        return symbols.containsKey(s) ? true : parent.symbolAccessible(s);
    }

    public void addTypeSymbol(String name) {
        symbols.put(name, new TypeSymbol(name));
    }

    public void addStaticSymbol(String name, String type) {
        symbols.put(name, new VarSymbol(name, type, Symbol.Kind.STATIC, staticOffset));
        staticOffset ++;
    }

    public void addFieldSymbol(String name, String type) {
        symbols.put(name, new VarSymbol(name, type, Symbol.Kind.FIELD, fieldOffset));
        fieldOffset ++;
    }

    public int getNumFields() {
        int count = 0;
        for (Symbol s : symbols.values()) {
            if (s.isField()) count += 1;
        }
        return count;
    }

    public void addFuncSymbol(FuncSymbol fs) {
        symbols.put(fs.name, fs);
    }

    public FuncSymbol getFuncSymbol(String name) {
        return ((FuncSymbol) symbols.get(name));
    }

    protected class SortVars implements Comparator<Symbol> {
        public int compare(Symbol a, Symbol b) {
            if (a.kind == b.kind && a instanceof VarSymbol && b instanceof VarSymbol) {
                return ((VarSymbol) a).getOffset() - ((VarSymbol) b).getOffset();
            }
            return a.kind.compareTo(b.kind);
        }
    }

    public String printString() {
        List<Symbol> vals = new ArrayList<>(symbols.values());
        Collections.sort(vals, new SortVars());
        String str = name + " - class - - - - - - - - - - - - - - - - - - - -";
        str = str.substring(0,49) + "\n|\n";
        for (Symbol s : vals) {
            str = str + "| " + s.printString();
        }
        str = str + " - - - - - - - - - - - - - - - - - - - - - - - - -\n";
        return str;
    }


}