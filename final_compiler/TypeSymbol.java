

public class TypeSymbol extends Symbol {

    public TypeSymbol(String name) {
        super(name, name, Symbol.Kind.TYPE);
    }

    public String printString() {
        return String.format("  %-8s || %-10s | %-8s | %-3s |\n", name, "x", "type", "x");
    }


}